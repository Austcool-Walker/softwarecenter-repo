run this command to add this repo to `/etc/apt/sources.list.d` 

first aquire GPG key using.
```
wget -qO - https://gitlab.com/Austcool-Walker/softwarecenter-repo/raw/master/ubuntu/9E82C33E.key | sudo apt-key add -
```

```
echo "deb https://gitlab.com/Austcool-Walker/softwarecenter-repo/raw/master/ubuntu ubuntu main" | sudo tee /etc/apt/sources.list.d/softwarecenter-repo.list
```


*I take no responisablity for the packages in this repo*
